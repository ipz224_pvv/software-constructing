﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_4.ChainOfResponsibility
{
    class GeneralSupportHandler : SupportHandler
    {
        public override string? HandleRequest(string request)
        {
            if (request == "3")
                return "Welcome to general support.";
            else
                return base.HandleRequest(request);
        }
    }
}
