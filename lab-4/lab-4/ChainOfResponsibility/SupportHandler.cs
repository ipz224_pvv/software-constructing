﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_4.ChainOfResponsibility
{
    abstract class SupportHandler
    {
        protected SupportHandler? NextHandler;

        public SupportHandler SetNext(SupportHandler handler)
        {
            NextHandler = handler;
            return NextHandler;
        }

        public virtual string? HandleRequest(string request)
        {
            return NextHandler?.HandleRequest(request);
        }
    }
}
