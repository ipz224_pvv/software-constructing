﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_4.ChainOfResponsibility
{
    class BillingSupportHandler : SupportHandler
    {
        public override string? HandleRequest(string request)
        {
            if (request == "2")
                return "Welcome to billing support.";
            else
                return base.HandleRequest(request);
        }
    }
}
