﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_4.ChainOfResponsibility
{

    class TechnicalSupportHandler : SupportHandler
    {
        public override string? HandleRequest(string request)
        {
            if (request == "1")
                return "Welcome to technical support.";
            else
                return base.HandleRequest(request);
        }
    }
}
