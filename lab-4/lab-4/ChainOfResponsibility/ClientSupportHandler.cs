﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_4.ChainOfResponsibility
{
    class ClientSupportHandler : SupportHandler
    {
        public override string? HandleRequest(string request)
        {
            if (request == "4")
                return "Welcome to client support.";
            else
                return base.HandleRequest(request);
        }
    }
}
