﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_4.Observer
{
    public class LightElementNode : LightNode
    {
        public string TagName { get; }
        public bool IsBlock { get; }
        public bool IsSelfClosing { get; }
        public List<string> CssClasses { get; }
        public List<LightNode> Children { get; }

        public LightElementNode(string tagName, bool isBlock, bool isSelfClosing, List<string> cssClasses, List<LightNode> children)
        {
            TagName = tagName;
            IsBlock = isBlock;
            IsSelfClosing = isSelfClosing;
            CssClasses = cssClasses;
            Children = children;
        }

        public override string OuterHTML
        {
            get
            {
                StringBuilder builder = new StringBuilder();
                builder.Append($"<{TagName}");

                if (CssClasses != null && CssClasses.Count > 0)
                {
                    builder.Append(" class=\"");
                    builder.Append(string.Join(" ", CssClasses));
                    builder.Append("\"");
                }

                builder.Append(">");

                if (!IsSelfClosing)
                {
                    foreach (var child in Children)
                    {
                        builder.Append(child.OuterHTML);
                    }

                    builder.Append($"</{TagName}>");
                }

                return builder.ToString();
            }
        }

        public override string InnerHTML
        {
            get
            {
                StringBuilder builder = new StringBuilder();

                foreach (var child in Children)
                {
                    builder.Append(child.OuterHTML);
                }

                return builder.ToString();
            }
        }
    }
}
