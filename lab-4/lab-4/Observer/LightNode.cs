﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_4.Observer
{
    public abstract class LightNode
    {
        public Dictionary<string, Subscriber> EventListeners { get; set; }

        public LightNode()
        {
            EventListeners = new Dictionary<string, Subscriber>();
        }

        public void AddEventListener(string eventName, Subscriber action)
        {
            if (EventListeners.ContainsKey(eventName))
            {
                EventListeners[eventName] += action;
            }
            else
            {
                EventListeners[eventName] = action;
            }
        }

        public void RemoveEventListener(string eventName, Subscriber action)
        {
            if (EventListeners.ContainsKey(eventName))
            {
                EventListeners[eventName] -= action;
            }
        }

        public void DispatchEvent(string eventName)
        {
            if (EventListeners.ContainsKey(eventName))
            {
                EventListeners[eventName]?.Invoke(this);
            }
        }

        public virtual string OuterHTML => "";
        public virtual string InnerHTML => "";
    }
}
