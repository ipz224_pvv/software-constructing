﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_4.Strategy
{
    class Image
    {
        private IImageLoadingStrategy _strategy;

        private string _href;

        public Image(string href)
        {
            _href = href;
        }

        public void SetStrategy(IImageLoadingStrategy strategy)
        {
            _strategy = strategy;
        }

        public void LoadImage()
        {
            _strategy.LoadImage(_href);
        }
    }
}
