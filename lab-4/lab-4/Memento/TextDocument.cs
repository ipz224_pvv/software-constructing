﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_4.Memento
{
    public class TextDocument
    {
        public string Content { get; set; }

        public TextDocument(string content)
        {
            Content = content;
        }

        public IMemento CreateMemento()
        {
            return new TextDocumentMemento(Content);
        }

        public void RestoreMemento(IMemento memento)
        {
            if (memento is TextDocumentMemento correctInstance)
                Content = correctInstance.Content;
            else
                throw new InvalidCastException("Invalid type of memento");
        }
    }
}
