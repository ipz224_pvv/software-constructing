﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_4.Memento
{
    public class TextEditor
    {
        private TextDocument _document;
        private Stack<IMemento> memento;

        public TextEditor(TextDocument document)
        {
            _document = document;
            memento = new();
        }

        public void Save()
        {
            memento.Push(_document.CreateMemento());
        }

        public void Undo()
        {
            if (memento.Count > 0)
            {
                _document.RestoreMemento(memento.Pop());
            }
            else
            {
                Console.WriteLine("Немає можливості відмінити подію.");
            }
        }

        public void Change(string text)
        {
            _document.Content = text;
            Save();
        }

        public void PrintContent()
        {
            Console.WriteLine("Зміст документа:");
            Console.WriteLine(_document.Content);
        }
    }
}
