﻿using lab_4.ChainOfResponsibility;
using lab_4.Mediator;
using lab_4.Memento;
using lab_4.Observer;
using lab_4.Strategy;
using System;
using System.Text;

class Program
{
    static void Main(string[] args)
    {
        //task1
        SupportHandler technicalSupport = new TechnicalSupportHandler();
        SupportHandler billingSupport = new BillingSupportHandler();
        SupportHandler generalSupport = new GeneralSupportHandler();
        SupportHandler clientSupport = new ClientSupportHandler();

        technicalSupport.SetNext(billingSupport).SetNext(generalSupport).SetNext(clientSupport);

        while (true)
        {
            Console.WriteLine("Welcome! Choose the type of support:");
            Console.WriteLine("1. Technical Support");
            Console.WriteLine("2. Billing Support");
            Console.WriteLine("3. General Support");
            Console.WriteLine("4. Client Support");

            var response = technicalSupport.HandleRequest(Console.ReadLine() ?? string.Empty);

            if(response is not null)
            {
                Console.WriteLine($"\n{response}\n");
                break;
            }

        }

        //task2
        CommandCentre controlTower = new CommandCentre();

        Runway runway1 = new Runway();
        Runway runway2 = new Runway();

        controlTower.RegisterRunway(runway1);
        controlTower.RegisterRunway(runway2);

        Aircraft aircraft1 = new Aircraft("Boeing 747");
        Aircraft aircraft2 = new Aircraft("Airbus A320");

        controlTower.RegisterAircraft(aircraft1);
        controlTower.RegisterAircraft(aircraft2);

        aircraft1.RequestToLand(runway1.Id);
        aircraft2.RequestToLand(runway1.Id);
        aircraft2.RequestToLand(runway2.Id);

        aircraft1.TakeOff();
        aircraft2.TakeOff();

        //task3 

        var text = new LightTextNode("hello world");

        text.AddEventListener("click", (object data) =>
        {
            Console.WriteLine("Клікнули на text!");
        });

        text.AddEventListener("mouseover", (object data) =>
        {
            Console.WriteLine("Навели курсор на text!");
        });

        text.DispatchEvent("click");

        text.DispatchEvent("mouseover");

        //task4

        IImageLoadingStrategy imageLoadingStrategy1 = new FileSystemImageLoadingStrategy();
        IImageLoadingStrategy imageLoadingStrategy2 = new NetworkImageLoadingStrategy();

        Image img = new Image("image.png");
        img.SetStrategy(imageLoadingStrategy1);
        img.LoadImage();
        img.SetStrategy(imageLoadingStrategy2);
        img.LoadImage();

        //task5
        var document = new TextDocument("Початковий текст. ");
        var editor = new TextEditor(document);

        editor.Save();

        editor.Change("Новий текст. ");

        editor.Undo();

        editor.PrintContent();

        editor.Undo();

        editor.PrintContent();

    }
}

