﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_4.Mediator
{
    interface IMediator
    {
        public void Send(string request, object data);
    }
}
