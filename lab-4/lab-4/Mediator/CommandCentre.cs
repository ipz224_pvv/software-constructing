﻿using DesignPatterns.Mediator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_4.Mediator
{
    class CommandCentre : IMediator
    {
        private List<Runway> _runways = new List<Runway>();
        private List<Aircraft> _aircrafts = new List<Aircraft>();

        public void RegisterRunway(Runway runway)
        {
            _runways.Add(runway);
            runway.SetMediator(this);
        }

        public void RegisterAircraft(Aircraft aircraft)
        {
            _aircrafts.Add(aircraft);
            aircraft.SetMediator(this);
        }

        public void Send(string request, object data)
        {
            switch (request)
            {
                case "TryToLand":
                    if (!RequestToLand((Guid)data))
                        throw new InvalidOperationException($"Runway {data} is busy");
                    break;
                case "TakeOff":
                    RequestToTakeOff((Guid)data);
                    break;

            }
        }

        public bool RequestToLand(Guid id)
        {
            foreach (var runway in _runways)
            {
                if (runway.Id == id)
                {
                    return runway.Land();
                }
            }
            return false;
        }

        public void RequestToTakeOff(Guid id)
        {
            foreach (var runway in _runways)
            {
                if (runway.Id == id)
                {
                    runway.TakeOff();
                }
            }
        }

    }
}
