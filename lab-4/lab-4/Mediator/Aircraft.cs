﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_4.Mediator
{
    class Aircraft
    {

        private IMediator? mediator;

        private Guid? runwayId;

        public string Name;

        public Aircraft(string name)
        {
            Name = name;
        }

        public void SetMediator(IMediator mediator)
        {
            this.mediator = mediator;
        }

        public void RequestToLand(Guid runwayId)
        {
            Console.WriteLine($"Aircraft {Name} is landing on {runwayId} runway.");
            try
            {
                mediator?.Send("TryToLand", runwayId);
                this.runwayId = runwayId;
                Console.WriteLine($"Aircraft {Name} is landed on {runwayId} runway.");
            }
            catch
            {
                Console.WriteLine($"Could not land, {runwayId} runway is busy.");
            }
        }

        public void TakeOff()
        {
            if (runwayId is null)
            {
                Console.WriteLine($"Aircraft {Name} is in air");
                return;
            }

            Console.WriteLine($"Aircraft {Name} is taking off from {runwayId} runway.");
            mediator?.Send("TakeOff", runwayId);
            Console.WriteLine($"Aircraft {Name} is taken off from {runwayId} runway.");
        }
    }
}
