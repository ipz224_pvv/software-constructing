﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_4.Mediator
{
    class Runway
    {
        private IMediator? mediator;

        public readonly Guid Id = Guid.NewGuid();
        public bool IsBusy { get; private set; }

        public void SetMediator(IMediator mediator)
        {
            this.mediator = mediator;
        }

        public bool IsAvailable()
        {
            return !IsBusy;
        }

        public bool Land()
        {
            if (!IsBusy)
            {
                IsBusy = true;
                return true;
            }
            return false;
        }

        public void TakeOff()
        {
            IsBusy = false;
        }
    }
}
