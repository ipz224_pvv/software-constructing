﻿using lab_1.Behaviour.Report.ReportData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_1.Behaviour.Report.ReportProvider
{
    
    public class ReportProvider : IReportProvider<IReportData>
    {
        public void Report(IReportData data)
        {
            Console.WriteLine(data.FormatReport());
        }

    }
}
