﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_1.Behaviour.Report.ReportData
{
    public interface IReportData
    {
        public string FormatReport();
    }
}
