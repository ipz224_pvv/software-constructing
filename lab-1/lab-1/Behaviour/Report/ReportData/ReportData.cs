﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_1.Behaviour.Report.ReportData
{
    
    public class ReportData : IReportData
    {
        public string Data { get; set; }
        public DateTime Date { get; set; }

        public string FormatReport()
        {
            return $"Report date - {Date}\n" + Data;
        }
    }
}
