﻿using lab_1.Behaviour.Report.ReportData;
using lab_1.Behaviour.Report.ReportProvider;
using lab_1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_1.Behaviour.Report
{
    public class Reporting
    {
        private Warehouse warehouse;
        private IReportProvider<IReportData> report;

        public Reporting(Warehouse warehouse, IReportProvider<IReportData> report)
        {
            this.warehouse = warehouse;
            this.report = report;
        }

        public void RegisterIncoming(Product product, int quantity)
        {
            var storedProduct = warehouse.Products.FirstOrDefault(pr => pr.Name.Equals(product.Name));

            if (storedProduct is null)
            {
                product.Quantity += quantity;
                warehouse.Products.Add(product);
            }
            else
            {
                storedProduct.Quantity += quantity;
            }

            report.Report(new ReportData.ReportData() { Date = DateTime.UtcNow, Data = $"Надходження: {quantity} одиниць товару '{product.Name}' на склад." });
        }

        public void RegisterOutgoing(Product product, int quantity)
        {
            if (product.Quantity < quantity)
            {
                throw new InvalidOperationException("Недостатня кількість товару на складі.");
            }

            var storedProduct = warehouse.Products.FirstOrDefault(pr => pr.Name.Equals(product.Name));

            if (storedProduct is null)
            {
                throw new InvalidDataException("Вказаний продукт не інує");
            }

            storedProduct.Quantity -= quantity;

            report.Report(new ReportData.ReportData() { Date = DateTime.UtcNow, Data = $"Відвантаження: {quantity} одиниць товару '{product.Name}' зі складу." });
        }

        public void GenerateInventoryReport()
        {
            report.Report(new ReportData.ReportData() { Date = DateTime.UtcNow, Data = "Звіт по інвентаризації:\n" + warehouse.ToString() });
        }
    }
}
