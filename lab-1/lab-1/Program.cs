﻿using lab_1.Behaviour.Report;
using lab_1.Behaviour.Report.ReportProvider;
using lab_1.Models;
using System;
using System.Collections.Immutable;
using System.Text;


class Program
{
    static void Main(string[] args)
    {
        Warehouse warehouse = new Warehouse();
        Reporting reporting = new Reporting(warehouse, new ReportProvider());

        Product book = new Product("Книга", new Money(50, 0), 10);
        warehouse.AddProduct(book);
        Product milk = new Product("Молоко", new Money(25, 0), 20);
        warehouse.AddProduct(milk);

        reporting.RegisterIncoming(book, 5);
        reporting.RegisterIncoming(milk, 10);

        reporting.RegisterOutgoing(milk, 5);

        reporting.GenerateInventoryReport();
    }
}
