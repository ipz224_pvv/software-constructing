﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_1.Models
{
    public class Product
    {
        public string Name { get; set; }
        public Money Price { get; set; }
        public int Quantity { get; set; }

        public Product(string name, Money price, int quantity)
        {
            Name = name;
            Price = price;
            Quantity = quantity;
        }

        public void DecreasePrice(decimal amount)
        {

            decimal TotalPrice = Price.TotalPrice;
            if (amount < 0)
            {
                throw new ArgumentException("Amount must be a positive number.");
            }

            if (TotalPrice < amount)
            {
                throw new ArgumentException("Amount must be less that total price.");
            }

            TotalPrice -= amount;

            Price.Cents = (int)(TotalPrice % 1);
            Price.Dollars = (int)Math.Floor(TotalPrice);

        }
    }

}
