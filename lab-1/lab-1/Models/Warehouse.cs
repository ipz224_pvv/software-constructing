﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_1.Models
{
    public class Warehouse
    {
        private List<Product> products;

        public List<Product> Products
        {
            get
            {
                return products;
            }
        }

        public Warehouse()
        {
            products = new List<Product>();
        }

        public void AddProduct(Product product)
        {
            products.Add(product);
        }

        public void RemoveProduct(Product product)
        {
            products.Remove(product);
        }

        public override string ToString()
        {
            StringBuilder stringBuilder = new StringBuilder();
            foreach (var product in products)
            {
                stringBuilder.AppendLine($"Назва: {product.Name}, Ціна: ${product.Price.Dollars}.{product.Price.Cents:00}, Кількість: {product.Quantity}");
            }
            return stringBuilder.ToString();
        }
    }

}
