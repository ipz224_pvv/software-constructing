﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_1.Models
{
    public class Money
    {
        private int dollars;
        private int cents;

        public Money(int dollars, int cents)
        {
            this.dollars = dollars;
            this.cents = cents;
        }

        public int Dollars
        {
            get { return dollars; }
            set { dollars = value; }
        }

        public int Cents
        {
            get { return cents; }
            set { cents = value; }
        }

        public decimal TotalPrice
        {
            get
            {
                return dollars + cents * 0.01m;
            }
        }

        public override string ToString()
        {
            return $"Сума: {dollars}.{cents:00}";
        }
    }
}
