# Principles

## Fail Fast

Я використала цей принцип у:

+ [Product](./Models/Product.cs)
+ [Reporting](./Behaviour/Report/Reporting.cs)

У класі Product у методі DecreasePrice при некоректній сумі ви кидаємо виняток. Така ж ситуація у класі Reporting у методі RegisterOutgoing: якщо кількість менша за наявну або продукта не існує - кидаємо виняток.

## Single Responsibility principle

Я використала цей принцип у:

+ [Money](./Models/Money.cs)
+ [Product](./Models/Product.cs)
+ [Warehouse](./Models/Warehouse.cs)
+ [Reporting](./Behaviour/Report/Reporting.cs)

Клас Money представляє лише гроші. Клас Product представляє продукт і дії над ним. Клас Warehouse представляє склад і дії з ним. Клас Reporting представляє функціонал для формування звітності.

## Open/Close Principle

Я використала цей принцип у:

+ [Reporting](./Behaviour/Report/Reporting.cs)

У класі Reporting є поле Report, тип якого є IReportProvider<IReportData>, який додає коду гнучності та надає можливість користувачу без зміни класу розширювати його функціонал.

## Interface segregation principle

Я використала цей принцип у:

+[IReportData](./Behaviour/Report/ReportData/IReportData.cs)

Я розділила 1 інтерфейс на 2: перший працює з даними (IReportData), а другий працює з відправкою даних (IReportProvider).

## Liskov Substitution principle

Я використала цей принцип у:

+[IReportProvider](./Behaviour/Report/ReportProvider/IReportProvider.cs)

У C# Liskov Substitution principle має дуже велике значення. Ми його  знаємо як коваріантність та контрваріантність. Я його застосувала у інтерфейсі IReportProvider. Це дає змогу присвоювати реалізацію цього інтерфейсу для класів-наслідників певного типу до типу інтерфейсу, в якому заданий цей тип.

## KISS 

Я використала цей принцип у:

+ [Product](./Models/Product.cs)

У класі Product у методі DecreasePrice я made code straigth. Мені треба повідомити про виключення - я його просто кидаю, не створюючи екстра класів для цього.

## YAGNI

Я створила лише те, що від мене потребували завдання і нічого зайвого, тому я за замовчуванням застосувала цей принцип.

