﻿using lab_3.Decorator.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_3.Decorator.Decorators
{
    class InventoryItem : InventoryDecorator
    {
        private string item;

        public InventoryItem(Hero hero, string item) : base(hero)
        {
            this.item = item;
        }

        public override string GetDescription()
        {
            return base.GetDescription() + ", " + item;
        }
    }
}
