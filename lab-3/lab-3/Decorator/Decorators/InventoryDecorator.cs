﻿using lab_3.Decorator.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_3.Decorator.Decorators
{
    abstract class InventoryDecorator : Hero
    {
        protected Hero hero;

        public InventoryDecorator(Hero hero)
        {
            this.hero = hero;
        }

        public override string GetDescription()
        {
            return hero.GetDescription();
        }
    }
}
