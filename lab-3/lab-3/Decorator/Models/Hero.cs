﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_3.Decorator.Models
{
    abstract class Hero
    {
        public abstract string GetDescription();
    }
}
