﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_3.Decorator.Models
{
    class Mage : Hero
    {
        public override string GetDescription()
        {
            return "Mage";
        }
    }
}
