﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_3.Bridge.Renders
{
    interface IRenderer
    {
        void RenderShape(string shape);
    }
}
