﻿using lab_3.Bridge.Renders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_3.Bridge.Figures
{
    class Triangle : Shape
    {
        public Triangle(IRenderer renderer) : base(renderer)
        {
        }

        public override void Draw()
        {
            renderer.RenderShape("Triangle");
        }
    }
}
