﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_3.Proxy.TextReader
{
    class SmartTextReader : ITextReader
    {
        public string[,] ReadText(string filePath)
        {
            string[] lines = File.ReadAllLines(filePath);
            int maxLength = lines.Max(line => line.Length);
            string[,] result = new string[lines.Length, maxLength];

            for (int i = 0; i < lines.Length; i++)
            {
                for (int j = 0; j < lines[i].Length; j++)
                {
                    result[i, j] = lines[i][j].ToString();
                }
            }

            return result;
        }
    }
}
