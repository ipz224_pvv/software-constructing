﻿using lab_3.Proxy.TextReader;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace lab_3.Proxy
{
    class SmartTextReaderLocker : ITextReader
    {
        private SmartTextReader reader;
        private Regex pattern;

        public SmartTextReaderLocker(string pattern)
        {
            reader = new SmartTextReader();
            this.pattern = new Regex(pattern);
        }

        public string[,] ReadText(string filePath)
        {
            if (pattern.IsMatch(filePath))
            {
                Console.WriteLine("Access denied!");
                return null;
            }
            else
            {
                return reader.ReadText(filePath);
            }
        }
    }
}
