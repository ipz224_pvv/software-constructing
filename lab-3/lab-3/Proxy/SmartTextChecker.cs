﻿using lab_3.Proxy.TextReader;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_3.Proxy
{
    class SmartTextChecker : ITextReader
    {
        private SmartTextReader reader;

        public SmartTextChecker()
        {
            reader = new SmartTextReader();
        }

        public string[,] ReadText(string filePath)
        {
            Console.WriteLine($"Opening file: {filePath}");
            string[,] result = reader.ReadText(filePath);
            Console.WriteLine($"Read {result.GetLength(0)} lines and {result.GetLength(1)} characters.");
            Console.WriteLine($"Closing file: {filePath}");

            return result;
        }
    }
}
