﻿using lab_3.Composite;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;

public class LightHtmlCacher
{
    private ConcurrentDictionary<string, LightNode> nodes = new ();

    public LightNode GetOrCreate(string key, Func<LightNode> callback)
    {
        try
        {
            return nodes[key];
        }
        catch
        {
            var value = callback();
            nodes[key] = value;
            return value;
        }
    }
}