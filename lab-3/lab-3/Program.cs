﻿using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Engines;
using BenchmarkDotNet.Running;
using lab_3.Adapter;
using lab_3.Bridge;
using lab_3.Bridge.Figures;
using lab_3.Bridge.Renders;
using lab_3.Composite;
using lab_3.Decorator;
using lab_3.Decorator.Decorators;
using lab_3.Decorator.Models;
using lab_3.Proxy;
using lab_3.Proxy.TextReader;
using System;

[MemoryDiagnoser]
public class Program
{

    [Benchmark]
    public void WithoutCache()
    {
        GetHtmlWithoutCache().GetAwaiter().GetResult().Consume(new());
    }

    [Benchmark]
    public void WithCache()
    {
        GetHtmlWithCache().GetAwaiter().GetResult().Consume(new());
    }


    public static async Task<List<LightNode>> GetHtmlWithoutCache()
    {
        List<LightNode> bookNodes = new List<LightNode>();
        LightHtmlCacher cacher = new LightHtmlCacher();

        using HttpClient client = new HttpClient();

        var bookContent = await client.GetStringAsync("https://www.gutenberg.org/cache/epub/1513/pg1513.txt");
        var bookLines = bookContent.Split('\n');

        foreach (var bookLine in bookLines)
        {
            if (string.IsNullOrWhiteSpace(bookLine))
                continue;

            if (bookLine.Length < 20)
                bookNodes.Add(
                    cacher.GetOrCreate(bookLine, () => new LightElementNode("h2", true, false, null, new List<LightNode> { new LightTextNode(bookLine) })));
            else if (char.IsWhiteSpace(bookLine[0]))
                bookNodes.Add(
                    cacher.GetOrCreate(bookLine, () => new LightElementNode("blockquote", true, false, null, new List<LightNode> { new LightTextNode(bookLine) })));
            else
                bookNodes.Add(
                    cacher.GetOrCreate(bookLine, () => new LightElementNode("p", true, false, null, new List<LightNode> { new LightTextNode(bookLine) })));
        }

        var bookHTML = new LightElementNode("html", true, false, null, new List<LightNode>
                {
                    new LightElementNode("head", true, false, null, new List<LightNode>
                    {
                        new LightElementNode("title", true, false, null, new List<LightNode> { new LightTextNode("Book HTML") })
                    }),
                    new LightElementNode("body", true, false, null, bookNodes)
                });

        return bookNodes;
    }

    public static async Task<List<LightNode>> GetHtmlWithCache()
    {
        List<LightNode> bookNodes = new List<LightNode>();

        using HttpClient client = new HttpClient();

        var bookContent = await client.GetStringAsync("https://www.gutenberg.org/cache/epub/1513/pg1513.txt");
        var bookLines = bookContent.Split('\n');

        foreach (var bookLine in bookLines)
        {
            if (string.IsNullOrWhiteSpace(bookLine))
                continue;

            if (bookLine.Length < 20)
                bookNodes.Add(new LightElementNode("h2", true, false, null, new List<LightNode> { new LightTextNode(bookLine) }));
            else if (char.IsWhiteSpace(bookLine[0]))
                bookNodes.Add(new LightElementNode("blockquote", true, false, null, new List<LightNode> { new LightTextNode(bookLine) }));
            else
                bookNodes.Add(new LightElementNode("p", true, false, null, new List<LightNode> { new LightTextNode(bookLine) }));
        }

        var bookHTML = new LightElementNode("html", true, false, null, new List<LightNode>
                {
                    new LightElementNode("head", true, false, null, new List<LightNode>
                    {
                        new LightElementNode("title", true, false, null, new List<LightNode> { new LightTextNode("Book HTML") })
                    }),
                    new LightElementNode("body", true, false, null, bookNodes)
                });

        return bookNodes;
    }

    static async Task Main(string[] args)
    {

        //task1
        Logger logger = new Logger();
        FileWriter fileWriter = new FileWriter();

        IFileLogger fileLogger = new FileLoggerAdapter(fileWriter);

        logger.Log("This is a log message");
        logger.Error("This is an error message");
        logger.Warn("This is a warning message");

        fileLogger.LogToFile("This is a log message for file");
        fileLogger.ErrorToFile("This is an error message for file");
        fileLogger.WarnToFile("This is a warning message for file");

        //task2
        Hero warrior = new Warrior();
        Hero mage = new Mage();
        Hero paladin = new Paladin();

        warrior = new InventoryItem(warrior, "Sword");
        warrior = new InventoryItem(warrior, "Shield");

        mage = new InventoryItem(mage, "Staff");
        mage = new InventoryItem(mage, "Robe");

        paladin = new InventoryItem(paladin, "Holy Sword");
        paladin = new InventoryItem(paladin, "Armor");

        Console.WriteLine(warrior.GetDescription());
        Console.WriteLine(mage.GetDescription());
        Console.WriteLine(paladin.GetDescription());

        //task3
        var circle = new Circle(new VectorRenderer());
        var square = new Square(new RasterRenderer());
        var triangle = new Triangle(new VectorRenderer());

        circle.Draw();
        square.Draw();
        triangle.Draw();

        //task4
        ITextReader textChecker = new SmartTextChecker();
        string[,] text = textChecker.ReadText("example.txt");

        ITextReader textLocker = new SmartTextReaderLocker(@"restricted");
        textLocker.ReadText("restricted_example.txt");
        textLocker.ReadText("another_example.txt");

        //task5
        var page = new LightElementNode("html", true, false, null, new List<LightNode>
        {
            new LightElementNode("head", true, false, null, new List<LightNode>
            {
                new LightElementNode("title", true, false, null, new List<LightNode>
                {
                    new LightTextNode("Заголовок сторінки")
                })
            }),
            new LightElementNode("body", true, false, null, new List<LightNode>
            {
                new LightElementNode("h1", true, false, null, new List<LightNode>
                {
                    new LightTextNode("Вітання!")
                }),
                new LightElementNode("p", true, false, null, new List<LightNode>
                {
                    new LightTextNode("Приклад сторінки, створеної за допомогою мови розмітки LightHTML.")
                })
            })
        });

        Console.WriteLine(page.OuterHTML);

        // task6
        BenchmarkRunner.Run<Program>();
    }
}