﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_3.Composite.Visitor
{
    public interface IVisitor
    {
        void VisitElementNode(LightElementNode elementNode);
        void VisitTextNode(LightTextNode textNode);
    }
}
