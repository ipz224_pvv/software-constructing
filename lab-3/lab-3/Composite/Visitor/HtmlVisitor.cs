﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_3.Composite.Visitor
{
    public class HtmlVisitor : IVisitor
    {
        public void VisitElementNode(LightElementNode elementNode)
        {
            Console.Write($"<{elementNode.TagName}");

            if (elementNode.CssClasses != null && elementNode.CssClasses.Count > 0)
            {
                Console.Write(" class=\"");
                Console.Write(string.Join(" ", elementNode.CssClasses));
                Console.Write("\"");
            }

            Console.Write(">");

            if (!elementNode.IsSelfClosing)
            {
                foreach (var child in elementNode.Children)
                {
                    child.Accept(this);
                }

                Console.Write($"</{elementNode.TagName}>");
            }
        }

        public void VisitTextNode(LightTextNode textNode)
        {
            Console.Write(textNode.Text);
        }
    }

}
