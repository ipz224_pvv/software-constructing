﻿using lab_3.Composite.State;
using lab_3.Composite.Visitor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_3.Composite
{
    public abstract class LightNode
    {
        public IState State { get; set; } = new EnabledState();
        public virtual string OuterHTML => string.Empty;
        public virtual string InnerHTML => string.Empty;

        public string GenerateHTML()
        {
            State.Enabled();
            StringBuilder builder = new StringBuilder(string.Empty);
            GenerateStartTag(builder);
            GenerateInnerHTML(builder);
            GenerateEndTag(builder);
            return builder.ToString();
        }

        protected abstract void GenerateStartTag(StringBuilder builder);
        protected abstract void GenerateInnerHTML(StringBuilder builder);
        protected abstract void GenerateEndTag(StringBuilder builder);
        public abstract void Accept(IVisitor visitor);
    }
}
