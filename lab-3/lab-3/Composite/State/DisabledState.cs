﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_3.Composite.State
{
    public class DisabledState : IState
    {
        public void Enabled()
        {
            throw new Exception("Element is diabled, therefore there is no way to get its html");
        }
    }
}
