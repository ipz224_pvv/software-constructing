﻿using lab_3.Composite.Iterator;
using lab_3.Composite.Visitor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_3.Composite
{
    public class LightTextNode : LightNode
    {
        public string Text { get; }

        public LightTextNode(string text)
        {
            Text = text;
        }

        public override string OuterHTML => Text;
        public override string InnerHTML => Text;

        protected override void GenerateStartTag(StringBuilder builder)
        {}

        protected override void GenerateInnerHTML(StringBuilder builder)
        {
            builder.Append(Text);
        }

        protected override void GenerateEndTag(StringBuilder builder)
        {}

        public ILightNodeIterator GetIterator()
        {
            return new LightTextNodeIterator(this);
        }

        public override void Accept(IVisitor visitor)
        {
            visitor.VisitTextNode(this);
        }
    }
}
