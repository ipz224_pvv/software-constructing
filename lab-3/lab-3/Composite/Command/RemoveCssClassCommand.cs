﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_3.Composite.Command
{
    public class RemoveCssClassCommand : ICommand
    {
        private readonly LightElementNode _element;
        private readonly string _cssClass;
        private bool _executed;

        public RemoveCssClassCommand(LightElementNode element, string cssClass)
        {
            _element = element;
            _cssClass = cssClass;
        }

        public void Execute()
        {
            _element.CssClasses.Remove(_cssClass);
            _executed = true;
        }

        public void Undo()
        {
            if (_executed)
            {
                _element.CssClasses.Add(_cssClass);
                _executed = false;
            }
        }
    }
}
