﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_3.Composite.Command
{
    public class CommandInvoker
    {
        private readonly List<ICommand> _commands = new List<ICommand>();
        private int _currentCommandIndex = -1;

        public void StoreAndExecute(ICommand command)
        {
            _commands.RemoveRange(_currentCommandIndex + 1, _commands.Count - _currentCommandIndex - 1);

            _commands.Add(command);
            _currentCommandIndex++;
            command.Execute();
        }

        public void Undo()
        {
            if (_currentCommandIndex >= 0)
            {
                _commands[_currentCommandIndex].Undo();
                _currentCommandIndex--;
            }
        }
    }

}
