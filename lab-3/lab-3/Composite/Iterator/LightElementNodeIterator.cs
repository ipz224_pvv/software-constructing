﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_3.Composite.Iterator
{
    public class LightElementNodeIterator : ILightNodeIterator
    {
        private List<LightNode> _children;
        private int _index = 0;

        public LightElementNodeIterator(List<LightNode> children)
        {
            _children = children;
        }

        public LightNode Next()
        {
            LightNode nextNode = _children[_index];
            _index++;
            return nextNode;
        }

        public bool HasNext()
        {
            return _index < _children.Count;
        }
    }
}
