﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_3.Composite.Iterator
{
    public class LightTextNodeIterator : ILightNodeIterator
    {
        private LightTextNode _text;
        private bool _hasNext = true;

        public LightTextNodeIterator(LightTextNode text)
        {
            _text = text;
        }

        public LightNode Next()
        {
            _hasNext = false;
            return _text;
        }

        public bool HasNext()
        {
            return _hasNext;
        }
    }
}
