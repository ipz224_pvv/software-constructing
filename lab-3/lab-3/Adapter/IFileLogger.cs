﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_3.Adapter
{
    public interface IFileLogger
    {
        void LogToFile(string message);
        void ErrorToFile(string message);
        void WarnToFile(string message);
    }
}
