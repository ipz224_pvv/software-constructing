﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_2.Prototype
{
    class Virus : ICloneable
    {
        public double Weight { get; set; }
        public int Age { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public List<Virus> Children { get; set; }

        public Virus()
        {
            Children = new List<Virus>();
        }

        public object Clone()
        {
            Virus clonedVirus = new Virus
            {
                Weight = this.Weight,
                Age = this.Age,
                Name = this.Name,
                Type = this.Type,
                Children = new List<Virus>()
            };

            foreach (var child in Children)
            {
                clonedVirus.Children.Add((Virus)child.Clone());
            }

            return clonedVirus;
        }
    }
}
