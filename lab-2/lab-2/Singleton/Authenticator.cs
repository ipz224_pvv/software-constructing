﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_2.Singleton
{
    class Authenticator
    {
        private static readonly object _lock = new object();
        private static Authenticator _instance;

        private Authenticator() { }

        public static Authenticator GetInstance()
        {
            lock (_lock)
            {
                return _instance ?? (_instance = new Authenticator());
            }
        }
    }
}
