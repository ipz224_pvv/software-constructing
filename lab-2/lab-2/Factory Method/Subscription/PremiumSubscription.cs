﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_2.Factory_Method
{
    class PremiumSubscription : Subscription
    {
        public PremiumSubscription()
        {
            MonthlyFee = 29.99;
            MinimumSubscriptionPeriod = 12;
            Channels = new List<string> { "Premium Channels" };
        }

        public override void Display()
        {
            Console.WriteLine("Premium Subscription:");
            Console.WriteLine($"Monthly Fee: ${MonthlyFee}");
            Console.WriteLine($"Minimum Subscription Period: {MinimumSubscriptionPeriod} month(s)");
            Console.WriteLine("Channels:");
            foreach (var channel in Channels)
            {
                Console.WriteLine(channel);
            }
        }
    }
}
