﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_2.Factory_Method
{
    abstract class Subscription
    {
        public double MonthlyFee { get; set; }
        public int MinimumSubscriptionPeriod { get; set; }
        public List<string> Channels { get; set; }

        public abstract void Display();
    }
}
