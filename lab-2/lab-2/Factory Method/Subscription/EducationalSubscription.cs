﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_2.Factory_Method
{
    class EducationalSubscription : Subscription
    {
        public EducationalSubscription()
        {
            MonthlyFee = 19.99;
            MinimumSubscriptionPeriod = 6;
            Channels = new List<string> { "Educational Channels" };
        }

        public override void Display()
        {
            Console.WriteLine("Educational Subscription:");
            Console.WriteLine($"Monthly Fee: ${MonthlyFee}");
            Console.WriteLine($"Minimum Subscription Period: {MinimumSubscriptionPeriod} month(s)");
            Console.WriteLine("Channels:");
            foreach (var channel in Channels)
            {
                Console.WriteLine(channel);
            }
        }
    }
}
