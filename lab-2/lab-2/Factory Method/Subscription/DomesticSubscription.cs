﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_2.Factory_Method
{
    class DomesticSubscription : Subscription
    {
        public DomesticSubscription()
        {
            MonthlyFee = 10.99;
            MinimumSubscriptionPeriod = 1;
            Channels = new List<string> { "Domestic Channels" };
        }

        public override void Display()
        {
            Console.WriteLine("Domestic Subscription:");
            Console.WriteLine($"Monthly Fee: ${MonthlyFee}");
            Console.WriteLine($"Minimum Subscription Period: {MinimumSubscriptionPeriod} month(s)");
            Console.WriteLine("Channels:");
            foreach (var channel in Channels)
            {
                Console.WriteLine(channel);
            }
        }
    }
}
