﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_2.Factory_Method
{
    class MobileApp : ISubscriptionFactory
    {
        public Subscription CreateSubscription()
        {
            return new EducationalSubscription();
        }
    }
}
