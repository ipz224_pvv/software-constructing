namespace lab_2.Builder;
class CharacterDirector
{
    private ICharacterBuilder characterBuilder;

    public CharacterDirector(ICharacterBuilder builder)
    {
        characterBuilder = builder;
    }

    public void ChangeBuilder(ICharacterBuilder builder)
    {
        characterBuilder = builder;
    }

    public void ConstructCharacter(string name, string height, string build, string hairColor, string eyeColor, string clothing, List<string> inventory)
    {

        characterBuilder.Reset();

        characterBuilder.SetName(name)
                       .SetHeight(height)
                       .SetBuild(build)
                       .SetHairColor(hairColor)
                       .SetEyeColor(eyeColor)
                       .SetClothing(clothing);

        foreach (var item in inventory)
        {
            characterBuilder.AddToInventory(item);
        }
    }

}