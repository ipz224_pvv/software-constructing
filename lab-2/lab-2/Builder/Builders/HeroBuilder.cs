namespace lab_2.Builder.Builders;

class HeroBuilder : ICharacterBuilder
{
    private Character hero = new Character();

    public ICharacterBuilder SetName(string name)
    {
        hero.Name = name;
        return this;
    }

    public ICharacterBuilder SetHeight(string height)
    {
        hero.Height = height;
        return this;
    }

    public ICharacterBuilder SetBuild(string build)
    {
        hero.Build = build;
        return this;
    }

    public ICharacterBuilder SetHairColor(string hairColor)
    {
        hero.HairColor = hairColor;
        return this;
    }

    public ICharacterBuilder SetEyeColor(string eyeColor)
    {
        hero.EyeColor = eyeColor;
        return this;
    }

    public ICharacterBuilder SetClothing(string clothing)
    {
        hero.Clothing = clothing;
        return this;
    }

    public ICharacterBuilder AddToInventory(string item)
    {
        hero.Inventory.Add(item);
        return this;
    }

    public Character Build()
    {
        return hero;
    }

    public void Reset()
    {
        hero = new();
    }
}