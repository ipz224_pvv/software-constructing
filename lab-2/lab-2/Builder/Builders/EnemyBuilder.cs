namespace lab_2.Builder.Builders;

class EnemyBuilder : ICharacterBuilder
{
    private Character enemy = new Character();

    public ICharacterBuilder SetName(string name)
    {
        enemy.Name = name;
        return this;
    }

    public ICharacterBuilder SetHeight(string height)
    {
        enemy.Height = height;
        return this;
    }

    public ICharacterBuilder SetBuild(string build)
    {
        enemy.Build = build;
        return this;
    }

    public ICharacterBuilder SetHairColor(string hairColor)
    {
        enemy.HairColor = hairColor;
        return this;
    }

    public ICharacterBuilder SetEyeColor(string eyeColor)
    {
        enemy.EyeColor = eyeColor;
        return this;
    }

    public ICharacterBuilder SetClothing(string clothing)
    {
        enemy.Clothing = clothing;
        return this;
    }

    public ICharacterBuilder AddToInventory(string item)
    {
        enemy.Inventory.Add(item);
        return this;
    }

    public Character Build()
    {
        return enemy;
    }

    public void Reset()
    {
        enemy = new();
    }
}