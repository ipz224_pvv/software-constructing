﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_2.Abstract_Factory
{
    interface IDeviceFactory
    {
        ILaptop CreateLaptop();
        ISmartphone CreateSmartphone();
    }
}
