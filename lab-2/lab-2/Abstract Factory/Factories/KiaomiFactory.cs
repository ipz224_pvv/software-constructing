﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_2.Abstract_Factory
{
    class KiaomiFactory : IDeviceFactory
    {
        public ILaptop CreateLaptop()
        {
            return new KiaomiLaptop();
        }

        public ISmartphone CreateSmartphone()
        {
            return new KiaomiSmartphone();
        }
    }
}
