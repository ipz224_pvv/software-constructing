﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_2.Abstract_Factory
{
    class BalaxyFactory : IDeviceFactory
    {
        public ILaptop CreateLaptop()
        {
            return new BalaxyLaptop();
        }

        public ISmartphone CreateSmartphone()
        {
            return new BalaxySmartphone();
        }
    }
}
