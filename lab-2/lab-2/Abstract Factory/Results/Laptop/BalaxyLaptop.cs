﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_2.Abstract_Factory
{
    class BalaxyLaptop : ILaptop
    {
        public void Display()
        {
            Console.WriteLine("Balaxy Laptop");
        }
    }
}
