﻿using System;
using System.Collections.Generic;
using lab_2.Abstract_Factory;
using lab_2.Builder.Builders;
using lab_2.Builder;
using lab_2.Factory_Method;
using lab_2.Prototype;
using lab_2.Singleton;

class Program
{
    static void Main(string[] args)
    {
        Console.WriteLine("\nTask 1\n");
        ISubscriptionFactory website = new WebSite();
        ISubscriptionFactory mobileApp = new MobileApp();
        ISubscriptionFactory managerCall = new ManagerCall();

        Subscription websiteSubscription = website.CreateSubscription();
        Subscription mobileAppSubscription = mobileApp.CreateSubscription();
        Subscription managerCallSubscription = managerCall.CreateSubscription();

        websiteSubscription.Display();
        Console.WriteLine();
        mobileAppSubscription.Display();
        Console.WriteLine();
        managerCallSubscription.Display();

        Console.WriteLine("\n-------------------------------\n");

        Console.WriteLine("\nTask 2\n");
        IDeviceFactory iProneFactory = new IProneFactory();
        ILaptop iProneLaptop = iProneFactory.CreateLaptop();
        ISmartphone iProneSmartphone = iProneFactory.CreateSmartphone();

        iProneLaptop.Display();
        iProneSmartphone.Display();

        Console.WriteLine("\n-------------------------------\n");

        Console.WriteLine("\nTask 3\n");
        Authenticator authenticator1 = Authenticator.GetInstance();
        Authenticator authenticator2 = Authenticator.GetInstance();

        Console.WriteLine(authenticator1 == authenticator2);  

        Console.WriteLine("\n-------------------------------\n");

        Console.WriteLine("\nTask 4\n");
        Virus originalVirus = new Virus
        {
            Weight = 1.5,
            Age = 5,
            Name = "Original Virus",
            Type = "Type A"
        };

        Virus childVirus1 = new Virus
        {
            Weight = 0.8,
            Age = 1,
            Name = "Child Virus 1",
            Type = "Type B"
        };

        Virus childVirus2 = new Virus
        {
            Weight = 0.9,
            Age = 2,
            Name = "Child Virus 2",
            Type = "Type B"
        };

        originalVirus.Children.Add(childVirus1);
        originalVirus.Children.Add(childVirus2);

        Virus clonedVirus = (Virus)originalVirus.Clone();

        Console.WriteLine(originalVirus.Name);
        Console.WriteLine(clonedVirus.Name);
        Console.WriteLine(originalVirus.Children[0].Name);
        Console.WriteLine(clonedVirus.Children[0].Name);
        Console.WriteLine(originalVirus.Children[1].Name);
        Console.WriteLine(clonedVirus.Children[1].Name);

        Console.WriteLine("\n-------------------------------\n");

        Console.WriteLine("\nTask 5\n");
        var heroBuilder = new HeroBuilder();
        var heroDirector = new CharacterDirector(heroBuilder);
        heroDirector.ConstructCharacter("Hero Steven", "6 feet", "Athletic", "Brown", "Blue", "Black suit", new List<string> { "Sword", "Shield" });
        var steven = heroBuilder.Build();

        var enemyBuilder = new EnemyBuilder();
        var enemyDirector = new CharacterDirector(enemyBuilder);
        enemyDirector.ConstructCharacter("Enemy Oliver", "6.5 feet", "Muscular", "Black", "Red", "Dark armor", new List<string> { "Magic staff" });
        var oliver = enemyBuilder.Build();

        Console.WriteLine("Steven:");
        steven.DisplayInfo();
        Console.WriteLine();

        Console.WriteLine("Oliver:");
        oliver.DisplayInfo();

    }
}

